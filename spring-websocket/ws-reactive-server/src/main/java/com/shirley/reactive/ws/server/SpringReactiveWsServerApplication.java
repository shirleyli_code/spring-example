package com.shirley.reactive.ws.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveWsServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveWsServerApplication.class, args);
	}

}

package com.shirley.ws.client.handler;

import com.shirley.ws.client.service.WsClientMsgSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class WebsocketClientHandler extends TextWebSocketHandler {

    private static AtomicInteger msgCount = new AtomicInteger(0);

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        log.info("Get message from server: {}", message.getPayload());
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.info("Connection established.");
        WsClientMsgSender.addSession(session);
    }


    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        log.info("Connection established.");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        log.info("Connection closed.");
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}

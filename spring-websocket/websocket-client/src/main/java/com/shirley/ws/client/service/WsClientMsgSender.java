package com.shirley.ws.client.service;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class WsClientMsgSender {

    private static ConcurrentHashMap<String, WebSocketSession> wsSessions = new ConcurrentHashMap<>();

    public static void addSession(WebSocketSession webSocketSession) {
        wsSessions.put(webSocketSession.getId(), webSocketSession);
    }

    public void sendMsg(String message) throws IOException {
        for (WebSocketSession session : wsSessions.values()) {
            session.sendMessage(new TextMessage(message));
        }
    }
}

package com.shirley.ws.client.config;

import com.shirley.ws.client.handler.WebsocketClientHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import java.net.URISyntaxException;

@Component
public class WsClientConf {

    @Bean
    public void init() throws URISyntaxException {
        WebSocketClient wsClient = new StandardWebSocketClient();
        wsClient.execute(new WebsocketClientHandler(), "ws://localhost:8080/ws");
    }
}

package com.shirley.ws.client;

import com.shirley.ws.client.service.WsClientMsgSender;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@SpringBootApplication
public class SpringWsClientApplication {

	public static void main(String[] args) throws IOException {
		ApplicationContext applicationContext = SpringApplication.run(SpringWsClientApplication.class, args);

		// Test send msg to server.
		WsClientMsgSender sender = applicationContext.getBean(WsClientMsgSender.class);

		System.out.println("Send message to server:");
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String msg = console.readLine();
			sender.sendMsg(msg);
		}
	}

}

package com.shirley.spring.tasks;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ScheduledTask {

    @Scheduled(cron ="*/5 * * * * *")
    public int ScheduledTaskOne() throws InterruptedException {
        log.info("------- ScheduledTaskOne start...");
        Thread.sleep(10000L);
        log.info("------- ScheduledTaskOne done.");
        DefaultListableBeanFactory f;
        return 1;
    }

    @Scheduled(fixedRate = 5 * 1000)
    public int ScheduledTaskTwo() throws InterruptedException {
        log.info("------- ScheduledTaskTwo start...");
        Thread.sleep(10000L);
        log.info("------- ScheduledTaskTwo done.");
        return 1;
    }

    @Scheduled(fixedDelay = 5 * 1000)
    public int ScheduledTaskThree() throws InterruptedException {
        log.info("------- ScheduledTaskThree start...");
        Thread.sleep(10000L);
        log.info("------- ScheduledTaskThree done.");
        return 1;
    }

}

package com.shirley.spring.tasks;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AsyncTask {
    @Async
    @Scheduled(cron = "* * * * * *")
    public void AsyncTaskOne() throws InterruptedException {
        log.info("------- AsyncTaskOne start...");
        Thread.sleep(10000L);
        log.info("------- AsyncTaskOne done.");
    }

    @Async
    @Scheduled(fixedDelay = 5 * 1000)
    public void AsyncTaskTwo() throws InterruptedException {
        log.info("------- AsyncTaskTwo start...");
        Thread.sleep(10000L);
        log.info("------- AsyncTaskTwo done.");
    }

    @Async
    @Scheduled(fixedRate = 5 * 1000)
    public void AsyncTaskThree() throws InterruptedException {
        log.info("------- AsyncTaskThree start...");
        Thread.sleep(10000L);
        log.info("------- AsyncTaskThree done.");
    }

    @Async("shirleyExecutor")
//    @Scheduled(cron = "0 0 * * * *")
    @Scheduled(cron = "@hourly")
    public void shirleyExeTask() throws InterruptedException {
        log.info("------- shirleyExeTask start...");
        Thread.sleep(10000L);
        log.info("------- shirleyExeTask done.");
    }


}

package com.shirley.spring.redis.message.imperative.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.connection.stream.Consumer;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.connection.stream.StreamReadOptions;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Slf4j
@Service
public class RedisImperativeRunner implements ApplicationRunner {

    private static final String LIST_KEY = "__list_key__";

    @Autowired
    RedisOperations<String, String> operations;
//    @Autowired
//    ReactiveRedisOperations<String, String> reactiveOperation;

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        subByList();
        subByStream();
//        subByPubSub();
//        subByNotification();
    }

    private void subByList() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                   String value = operations.opsForList().leftPop(LIST_KEY, Duration.ZERO);
                   log.info("Get value: {}", value);
                }
            }
        };
        thread.start();
    }

    private void subByNotification() {

    }

    private void subByPubSub() {
    }

    private void subByStream() {
        // Read message through RedisTemplate

    }


}

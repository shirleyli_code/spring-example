package com.shirley.spring.redis.message.reactive;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Slf4j
@Service
public class RedisReactiveRunner implements ApplicationRunner {

    private static final String REACTIVE_LIST_KEY = "__reactive_list_key__";

    @Autowired
    ReactiveRedisOperations<String, String> reactiveOperation;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        subByList();
//        subByStream();
//        subByPubSub();
//        subByNotification();
    }

    private void subByList() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    reactiveOperation.opsForList()
                            .leftPop(REACTIVE_LIST_KEY, Duration.ZERO)
                            .doOnNext(val -> log.info("Get list value by reactive way: {}", val))
                            .block();
                    log.info("----------");
                }
            }
        };
        thread.start();

    }
    private void subByNotification() {

    }

    private void subByPubSub() {
    }

    private void subByStream() {
    }


}

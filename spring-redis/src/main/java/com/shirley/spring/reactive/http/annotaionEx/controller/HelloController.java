package com.shirley.spring.reactive.http.annotaionEx.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String handle() {
        return "Hello Shirley";
    }
}

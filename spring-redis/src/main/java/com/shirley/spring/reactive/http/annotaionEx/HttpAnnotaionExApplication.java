package com.shirley.spring.reactive.http.annotaionEx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpAnnotaionExApplication {
    public static void main(String[] args) {
        SpringApplication.run(HttpAnnotaionExApplication.class, args);
    }
}

package com.shirley.amqp.workq;

import com.shirley.amqp.simple.SimpleSender;
import com.shirley.amqp.workq.conf.RabbitmqWorkQModelConf;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WorkQSenderTest {

    @Autowired
    private WorkQSender workQSender;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Test
    public void testSendMsg() throws Exception {
        for (int i = 0; i < 100; i++) {
//            workQSender.sendMsg("Hello rabbitmq work queue model, No." + i);
            rabbitTemplate.convertAndSend("ex.direct", "key1", "Hello " + i);

        }
        // wait Receiver get message.
        Thread.sleep(10000);
    }

}

package com.shirley.amqp.simple;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SimpleSenderTest {

    @Autowired
    private SimpleSender simpleSender;

    @Test
    public void testSendMsg() throws Exception {
        simpleSender.sendMsg("Hello rabbitmq simple model.");
        // wait Receiver get message.
        Thread.sleep(10000);
    }

}

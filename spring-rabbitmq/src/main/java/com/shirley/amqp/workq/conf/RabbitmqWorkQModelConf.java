package com.shirley.amqp.workq.conf;

import com.shirley.amqp.workq.WorkQReceiver;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqWorkQModelConf {

    public static final String workQModelQueueName = "queue.workq";
    @Bean("workqQueue")
    Queue workqQueue() {
        return new Queue(workQModelQueueName, false);
    }

    @Bean
    Binding workqModelBinding(@Qualifier("workqQueue") Queue workqQueue) {
        return BindingBuilder.bind(workqQueue).to(DirectExchange.DEFAULT).withQueueName();
    }

    @Bean("workQContainer")
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             @Qualifier("workQListenerAdapter")MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(workQModelQueueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean("workQListenerAdapter")
    MessageListenerAdapter listenerAdapter(WorkQReceiver workQReceiver) {
        return new MessageListenerAdapter(workQReceiver, "receiveMessage");
    }
}

package com.shirley.amqp.workq;

import com.shirley.amqp.workq.conf.RabbitmqWorkQModelConf;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class WorkQSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMsg(String message) throws Exception {
        log.info("Sending message...");
        rabbitTemplate.convertAndSend("ex.direct", RabbitmqWorkQModelConf.workQModelQueueName, message);
    }

}

package com.shirley.amqp.simple;

import com.shirley.amqp.simple.conf.RabbitmqSimpleModelConf;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SimpleSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMsg(String message) throws Exception {
        log.info("Sending message...");
        rabbitTemplate.convertAndSend("", RabbitmqSimpleModelConf.simpleModelQueueName, message);
    }

}

package com.shirley.amqp.simple.conf;

import com.shirley.amqp.simple.SimpleReceiver;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqSimpleModelConf {

    public static final String simpleModelQueueName = "queue.simple";
    @Bean("simpleQueue")
    Queue simpleQueue() {
        return new Queue(simpleModelQueueName, false);
    }

    @Bean
    Binding simpleModelBinding(@Qualifier("simpleQueue") Queue simpleQueue) {
        return BindingBuilder.bind(simpleQueue).to(DirectExchange.DEFAULT).withQueueName();
    }

    @Bean("simpleContainer")
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             @Qualifier("simpleListenerAdapter") MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(simpleModelQueueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean("simpleListenerAdapter")
    MessageListenerAdapter listenerAdapter(SimpleReceiver simpleReceiver) {
        return new MessageListenerAdapter(simpleReceiver, "receiveMessage");
    }
}

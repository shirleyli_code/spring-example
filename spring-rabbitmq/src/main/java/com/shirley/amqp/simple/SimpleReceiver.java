package com.shirley.amqp.simple;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

@Service
@Slf4j
public class SimpleReceiver {

    private final CountDownLatch latch = new CountDownLatch(1);

    public void receiveMessage(String message) {
        log.info("Simple Model - Received msg: {}", message);
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}
